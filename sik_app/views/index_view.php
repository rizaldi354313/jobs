<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view('common/meta_tags'); ?>
  <meta name="keywords" content="jobs in USA, government jobs, online jobs in USA, latest jobs in USA,  job in USA, latest jobs">
  <title><?php echo $title;?></title>
  <?php $this->load->view('common/before_head_close'); ?>

</head>
<body class="theme-style-1">
  <?php $this->load->view('common/after_body_open'); ?>
  <div id="wrapper">
    <!--Header-->
    <?php $this->load->view('common/header'); ?>
    <!--/Header--> 
    <!--Search Block-->

    <div class="banner-outer">

      <!-- <div id="banner" class="element">  -->
<!--        <?php 
       $slider_db = $this->db->get_where("pp_sliders",array("sts"=>"active")); 
       if($slider_db->num_rows() > 0){
        foreach($slider_db->result() as $slidkey=>$slidval){
          $active = "";
          if($slidkey == 0) $active = "active";
          ?>    
          <img src="<?php echo base_url($slidval->image_url); ?>" data-color="lightblue" alt="<?php echo $slidkey + 1; ?> Image">
          <?php
        }
      }
      ?> -->
      <div class="w3-content w3-display-container">
        <?php 
        $slider_db = $this->db->get_where("pp_sliders",array("sts"=>"active")); 
        if($slider_db->num_rows() > 0){
          foreach($slider_db->result() as $slidkey=>$slidval){
            $active = "";
            if($slidkey == 0) $active = "active";
            ?>    
            <!-- <img src="<?php echo base_url($slidval->image_url); ?>" data-color="lightblue" alt="<?php echo $slidkey + 1; ?> Image"> -->
            <img class="mySlides" src="<?php echo base_url($slidval->image_url); ?>" style="width:100%" alt="<?php echo $slidkey + 1; ?> Image">
            <?php
          }
        }
        ?>
      </div>
      <!-- </div> -->

      <div class="caption" style="margin-top: 100px;">

        <button class="w3-button w3-black w3-display-left" style="z-index: 9999;" onclick="plusDivs(-1)">&#10094;</button>
        <button class="w3-button w3-black w3-display-right" style="z-index: 9999;" onclick="plusDivs(1)">&#10095;</button>
        <div class="holder">

          <h1>Search Online Jobs or Hire Employees!</h1>

          <?php echo form_open_multipart('job_search/search',array('name' => 'jsearch', 'id' => 'jsearch'));?>

          <div class="container">

            <div class="row">

              <div class="col-md-6 col-sm-6">

                <input type="text"  name="job_params" id="job_params"  placeholder="Enter Job Title, Skills or Company">

              </div>

              <div class="col-md-5 col-sm-5">

                <select class="col-md-12 col-sm-12 col-xs-12" name="jcity" id="jcity" style="padding:0 20px">      	
                  <option value="" selected>Pilih Kota Tujuan</option>
                  <?php if($cities_res): foreach($cities_res as $cities):?>
                    <option value="<?php echo $cities->city_name;?>"><?php echo $cities->city_name;?></option>
                  <?php endforeach; endif;?>
                </select>

              </div>

              <!--div class="col-md-3 col-sm-3">

                <input type="text" placeholder="Category">

              </div -->

              <div class="col-md-1 col-sm-1">

                <button type="submit"><i class="fa fa-search"></i></button>

              </div>

            </div>

          </div>

          <?php echo form_close();?>

          <div class="banner-menu">

          <!--ul>

            <li><a href="#">San Francisco</a></li>

            <li><a href="#">Palo Alto</a></li>

            <li><a href="#">Mountain View</a></li>

            <li><a href="#">Sacramento</a></li>

            <li><a href="#">New York</a></li>

            <li><a href="#">United Kindom</a></li>

            <li><a href="#">Asia Pacific</a></li>

          </ul -->

        </div>

        <div class="btn-row"> <a href="job-seekers.html"><i class="fa fa-user"></i>I’m a Jobseeker</a> <a href="employers.html"><i class="fa fa-building-o"></i>I’m an Employer</a> </div>

      </div>

    </div>

    <div class="browse-job-section">

      <div class="container">

        <div class="holder"> <a href="<?php echo base_url("search-jobs"); ?>" class="btn-brows">Browse Jobs</a> <strong class="title">Finds Jobs in San Francisco, Palo Alto, Mountain View, Sacramento, New York, United Kindom, Asia Pacific</strong> </div>

      </div>

    </div>

  </div>

  <!--BANNER END--> 

  

  <!--MAIN START-->

  <div id="main"> 

    <!--POPULAR JOB CATEGORIES START-->

    <section class="popular-categories">

      <div class="container">

        <div class="clearfix">

          <h2>Popular Jobs Categories</h2>

          <a href="<?php echo base_url("search-jobs"); ?>" class="btn-style-1">Explore All Jobs</a> </div>
          <div class="row">
           <?php 

           if($industries->num_rows() > 0){
            foreach($industries->result() as $inkeys=>$inval){
             $jml_db = $this->db->get_where("pp_post_jobs",array("industry_ID"=>$inval->ID,"sts"=>"active")); 

             if($inkeys % 4 == 0){
              echo "</div><div class='row'>";

            }
            ?>
            <div class="col-md-3 col-sm-6">
              <div class="box"> 			
               <img src="<?php echo base_url("public/assets")."/"; ?>images/categories-icon-1.png" alt="img">
               <h4><a href="<?php echo base_url("industry/".$inval->slug); ?>"><?php echo $inval->industry_name ?></a></h4>
               <strong><?php echo $jml_db->num_rows(); ?> Jobs</strong>
               <p>Available in Design &amp; Multimedia</p>
             </div>
           </div>
           <?php
         }
       }
       ?>
     </div>


   </div>

 </section>

 <!--POPULAR JOB CATEGORIES END--> 



 <!--RECENT JOB SECTION START-->

 <section class="recent-row padd-tb">

  <div class="container">

    <div class="row">

      <div class="col-md-9 col-sm-8">

        <div id="content-area">

          <h2>Recent Hot Jobs</h2>

          <ul id="myList">
            <?php	
            if($latest_jobs_result):
              foreach($latest_jobs_result as $row_latest_jobs):
               $job_title = ellipsize(humanize($row_latest_jobs->job_title),34,1);
               $image_name = ($row_latest_jobs->company_logo)?$row_latest_jobs->company_logo:'no_logo.jpg';
               ?>
               <li>
                <div class="box">
                 <div class="thumb"><a href="<?php echo base_url('company/'.$row_latest_jobs->company_slug);?>" title="Jobs in <?php echo $row_latest_jobs->company_name;?>" class="thumbnail"><img src="<?php echo base_url('public/uploads/employer/thumb/'.$image_name);?>" alt="<?php echo base_url('company/'.$row_latest_jobs->company_slug);?>" /></a></div>
                 <div class="text-col">
                  <h4><a href="<?php echo base_url('jobs/'.$row_latest_jobs->job_slug);?>" class="jobtitle" title="<?php echo $row_latest_jobs->job_title;?>"><?php echo $job_title;?></a></h4> 
                  <a href="<?php echo base_url('company/'.$row_latest_jobs->company_slug);?>" title="Jobs in <?php echo $row_latest_jobs->company_name;?>"><?php echo $row_latest_jobs->company_name;?></a> &nbsp;-&nbsp; <i class="fa fa-map-marker"></i> <?php echo $row_latest_jobs->city;?></span>
                  <a class="btn-1 btn-color-2 ripple" href="<?php echo base_url('jobs/'.$row_latest_jobs->job_slug.'?apply=yes');?>" class="applybtn" title="<?php echo $row_latest_jobs->industry_name.' Job in '.$row_latest_jobs->city;?>">Lamar</a> 
                </div>
              </div>
            </li>
            <?php
          endforeach;
        endif;

        ?>

                <!-- li>

                  <div class="box">

                    <div class="thumb"><a href="#"><img src="<?php #echo base_url("public/assets")."/"; ?>images/recent-job-thumb-3.jpg" alt="img"></a></div>

                    <div class="text-col">

                      <h4><a href="#">Instructor Classroom Head</a></h4>

                      <p>School of Design and Multimedia</p>

                      <a href="#" class="text"><i class="fa fa-map-marker"></i>Nationwide</a> <a href="#" class="text"><i class="fa fa-calendar"></i>Dec 30, 2015 - Feb 20, 2016 </a> </div>

                    <strong class="price"><i class="fa fa-money"></i>$30/hr - $40/hr</strong> <a href="#" class="btn-1 btn-color-3 ripple">Freelance</a> </div>

                  </li -->

                </ul>
                <!--div id="loadMore"> <a href="#" class="load-more"><i class="fa fa-user"></i>Load More Jobs</a></div -->
              </div>

            </div>

            <div class="col-md-3 col-sm-4">

              <h2>Featured Jobs</h2>

              <aside>

                <div class="sidebar">
                  <?php
                  if($featured_job_result):
                    foreach($featured_job_result as $row_featured_job):
                     $image_name = ($row_featured_job->company_logo)?$row_featured_job->company_logo:'no_logo.jpg';
                     ?>
                     <div class="box">
                       <div class="thumb"> 
                        <a href="#">
                          <img src="<?php echo base_url("public/assets")."/"; ?>images/features-img-1.jpg" alt="img">
                        </a>
                        <div class="caption">
                         <img src="<?php echo base_url('public/uploads/employer/thumb/'.$image_name);?>" alt="<?php echo base_url('company/'.$row_latest_jobs->company_slug);?>" />
                       </div>	
                     </div>

                     <div class="text-box"> <a class="btn-time" href="<?php echo base_url('company/'.$row_featured_job->company_slug);?>" title="Jobs in <?php echo $row_featured_job->company_name;?>"><?php echo $row_featured_job->company_name;?></a>
                       <h4><a href="<?php echo base_url('jobs/'.$row_featured_job->job_slug);?>" title="<?php echo $row_featured_job->job_title;?>"><?php echo $row_featured_job->job_title;?></a></h4> 
                       <a href="#" class="text">
                         <i class="fa fa-map-marker"></i> <?php echo $row_featured_job->city;?>
                       </a>
                       <a href="#" class="text">
                         <i class="fa fa-calendar"></i>Due to <?php echo date_formats($row_latest_jobs->last_date, 'M d, Y');?>
                       </a>
                     </div>
                   </div>
                 <?php endforeach; endif; ?>



               </div>

             </aside>

           </div>

         </div>

       </div>

     </section>

     <!--RECENT JOB SECTION END--> 



     <!--CALL TO ACTION SECTION START-->

     <section class="call-action-section">

      <div class="container">

        <div class="text-box">

          <h2>Better Results with Standardized Hiring Process</h2>

          <p>Your quality of hire increases when you treat everyone fairly and equally. Having multiple recruiters

          working on your hiring is beneficial.</p>

        </div>

        <a href="#" class="btn-get">Get Registered &amp; Try Now</a> </div>

      </section>

      <!--CALL TO ACTION SECTION END--> 



      <!--PRICE TABLE STYLE 1 START-->

      <section class="price-table">

        <div class="container">

          <h2>Job Plans &amp; Pricing</h2>

          <div class="box">

            <h4>Free Package</h4>

            <strong class="amount"><span>$</span>29.00<sub>/mo</sub></strong>

            <ul>

              <li>Jobs are posted for 30 days</li>

              <li>One Time Fee</li>

              <li>This Plan Includes 1 Job</li>

              <li>Non-Highlighted Post</li>

              <li>Posted For 30 Days</li>

            </ul>

            <a href="#" class="btn-choose btn-color-2">Choose Plan</a> </div>

            <div class="box box-colo-2">

              <div class="head">Most Popular</div>

              <h4>Starter Package</h4>

              <strong class="amount"><span>$</span>59.00<sub>/mo</sub></strong>

              <ul>

                <li>Unlimited number of jobs</li>

                <li>Jobs are posted for 90 days</li>

                <li>One Time Fee</li>

                <li>This Plan Includes 50 Jobs</li>

                <li>Highlighted Job Post</li>

                <li>Posted For 60 Days</li>

              </ul>

              <a href="#" class="btn-choose btn-color-2">Choose Plan</a> </div>

              <div class="box box-colo-3">

                <h4>Enterprise</h4>

                <strong class="amount"><span>$</span>79.00<sub>/mo</sub></strong>

                <ul>

                  <li>Jobs are posted for 150 day</li>

                  <li>One Time Fee</li>

                  <li>This Plan Includes Unlimited Jobs</li>

                  <li>Featured or Highlighted Post</li>

                  <li>Posted For 120 Days</li>

                </ul>

                <a href="#" class="btn-choose btn-color-2">Choose Plan</a> </div>

              </div>

            </section>

            <!--PRICE TABLE STYLE 1 END--> 


            <!--POST AREA START-->

            <section class="post-section padd-tb">

              <div class="container">

                <div class="row">

                  <div class="col-md-4 col-sm-4">

                    <div class="post-box">

                      <div class="frame"><a href="#"><img src="<?php echo base_url("public/assets")."/"; ?>images/blog/post-img-1.jpg" alt="img"></a></div>

                      <div class="text-box"> <strong class="date"><i class="fa fa-calendar"></i>Mar 17, 2016</strong>

                        <h4><a href="#">Middle Class jobs are being replaced

                        by burger-flipping, retail sales.</a></h4>

                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusant doloremque laudantium, totam rem aperiam eaque ipsa quae.</p>

                        <div class="thumb"><a href="#"><img src="<?php echo base_url("public/assets")."/"; ?>images/admin-img.jpg" alt="img"></a></div>

                        <strong class="name"><span>By</span> Anna Smith</strong> </div>

                      </div>

                    </div>

                    <div class="col-md-4 col-sm-4">

                      <div class="post-box">

                        <div class="frame"><a href="#"><img src="<?php echo base_url("public/assets")."/"; ?>images/blog/post-img-2.jpg" alt="img"></a></div>

                        <div class="text-box"> <strong class="date"><i class="fa fa-calendar"></i>Feb 20, 2016 </strong>

                          <h4><a href="#">There are many variations of passage

                          of Lorem Ipsum available.</a></h4>

                          <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusant doloremque laudantium, totam rem aperiam eaque ipsa quae.</p>

                          <div class="thumb"><a href="#"><img src="<?php echo base_url("public/assets")."/"; ?>images/admin-img.jpg" alt="img"></a></div>

                          <strong class="name"><span>By</span> Anna Smith</strong> </div>

                        </div>

                      </div>

                      <div class="col-md-4 col-sm-4">

                        <div class="post-box">

                          <div class="frame"><a href="#"><img src="<?php echo base_url("public/assets")."/"; ?>images/blog/post-img-3.jpg" alt="img"></a></div>

                          <div class="text-box"> <strong class="date"><i class="fa fa-calendar"></i>Feb 09, 2016</strong>

                            <h4><a href="#">But I must explain to you how all this

                            mistaken idea of denouncing pleasure.</a></h4>

                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusant doloremque laudantium, totam rem aperiam eaque ipsa quae.</p>

                            <div class="thumb"><a href="#"><img src="<?php echo base_url("public/assets")."/"; ?>images/admin-img.jpg" alt="img"></a></div>

                            <strong class="name"><span>By</span> Anna Smith</strong> </div>

                          </div>

                        </div>

                      </div>

                    </div>

                  </section>

                  <!--POST AREA END--> 

                </div>

                <!--MAIN END--> 



                <!--/Search Block--> 

                <!--Latest Jobs Block-->

                <!--Footer-->
                <?php $this->load->view('common/footer'); ?>
              </div>
              <?php $this->load->view('common/before_body_close'); ?>

            </body>
            </html>