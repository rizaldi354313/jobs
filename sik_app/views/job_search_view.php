<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('common/meta_tags'); ?>
	<meta name="keywords" content="<?php echo $param;?> Jobs" />
	<meta name="description" content="<?php echo $param;?> Jobs ,Find best Jobs. Jobs at <?php echo SITE_NAME;?>." />
	<title><?php echo $title;?></title>
	<?php $this->load->view('common/before_head_close'); ?>
</head>
<body class="theme-style-1">
	<?php $this->load->view('common/after_body_open'); ?>
	<!--Header-->
	<?php $this->load->view('common/header'); ?>
	<!--/Header--> 
	<section id="inner-banner">

		<div class="container">

			<h1>Latest Jobs</h1>

		</div>

	</section>
	<section class="popular-job-caregries popular-categories candidates-listing">

		<div class="holder">

			<div class="container">

				<h4>Popular Job Categories</h4>

				<div id="popular-job-slider" class="owl-carousel owl-theme">

					<div class="item">

						<div class="box"> <img alt="img" src="<?php echo base_url('') ?>public/images/categories-icon-1.png">

							<h4><a href="#">Art, Design &amp; Multimedia</a></h4>

							<strong>6,213 Jobs</strong>

							<p>Available in Design &amp; Multimedia</p>

						</div>

					</div>

					<div class="item">

						<div class="box"> <img alt="img" src="<?php echo base_url('') ?>public/images/categories-icon-2.png">

							<h4><a href="#">Restaurant, Food, Hotels</a></h4>

							<strong>3,750 Jobs</strong>

							<p>Available in Restaurant &amp; Hotels</p>

						</div>

					</div>

					<div class="item">

						<div class="box"> <img alt="img" src="<?php echo base_url('') ?>public/images/categories-icon-3.png">

							<h4><a href="#">Transporation</a></h4>

							<strong>1,265 Jobs</strong>

							<p>Available in Transportation</p>

						</div>

					</div>

					<div class="item">

						<div class="box"> <img alt="img" src="<?php echo base_url('') ?>public/images/categories-icon-4.png">

							<h4><a href="#">Healthcare &amp; Medicine</a></h4>

							<strong>5,913 Jobs</strong>

							<p>Available in Medical</p>

						</div>

					</div>

					<div class="item">

						<div class="box"> <img alt="img" src="<?php echo base_url('') ?>public/images/categories-icon-5.png">

							<h4><a href="#">Software &amp; Development</a></h4>

							<strong>7,418 Jobs</strong>

							<p>Available in IT</p>

						</div>

					</div>

					<div class="item">

						<div class="box"> <img alt="img" src="<?php echo base_url('') ?>public/images/categories-icon-6.png">

							<h4><a href="#">Accounting &amp; Finance</a></h4>

							<strong>8,045 Jobs</strong>

							<p>Available in Accounts &amp; Finance</p>

						</div>

					</div>

					<div class="item">

						<div class="box"> <img alt="img" src="<?php echo base_url('') ?>public/images/categories-icon-7.png">

							<h4><a href="#">Education &amp; Academia</a></h4>

							<strong>3,891 Jobs</strong>

							<p>Available in Education</p>

						</div>

					</div>

					<div class="item">

						<div class="box"> <img alt="img" src="<?php echo base_url('') ?>public/images/categories-icon-8.png">

							<h4><a href="#">Construction, Engineering</a></h4>

							<strong>9,942 Jobs</strong>

							<p>Available in Architect</p>

						</div>

					</div>

				</div>

			</div>

		</div>

	</section>

	<div class="main">
		<!--Search Block-->
		<section class="candidates-search-bar">
			<div class="container">
				<?php echo form_open_multipart('job_search',array('name' => 'jsearch', 'id' => 'jsearch'));?>            
				<div class="row">
					<div class="col-md-5 col-sm-5">            
						<input type="text" name="job_params" id="job_params" class="form-control" placeholder="Title Pekerjaan / Skill" value="<?php echo $param;?>" />
					</div>
					<div class="col-md-5 col-sm-5">

						<select class="col-md-12 col-sm-12 col-xs-12" name="jcity" id="jcity" style="padding:0 20px">      	
							<option value="" selected>Pilih Kota Tujuan</option>
							<?php if($cities_res): foreach($cities_res as $cities):?>
								<option value="<?php echo $cities->city_name;?>"><?php echo $cities->city_name;?></option>
							<?php endforeach; endif;?>
						</select>

					</div>
					<div class="col-md-2 col-sm-2">
						<button type="submit" name="job_submit" class="btn btn-primary" id="job_submit" ><i class="fa fa-search"></i></button>
					</div>
				</div>
				<?php echo form_close();?> 
			</div>
		</section>
		<!--/Search Block--> 
		<!--Latest Jobs Block-->
		<section class="recent-row padd-tb">
			<div class="container">
				<div class="row"> 
					<!--Left Col-->
					<?php 
					// $col = '10';
					// if($this->uri->segment(1)!='search'){
					// 	if($result){
					// 		$col = '9';
					// 		$this->load->view('common/left_job_search');
					// 	}
					// }
					?>    
					<!--Mid Col-->

					<div class="col-md-<?php echo $col;?> col-sm-8"> 

						<!--Jobs List -->
						<div class="check-filter">
							<form action="#" >
								<div class="row">
									<div class="col-md-12">
									</div>
								</div>
								<div class="row">
									<div class="col-md-6"><b><?php echo $param;?> Lowongan</b></div>
									<div class="col-md-6 text-right"><strong>Lowongan <?php echo $from_record.' - '.$page;?> dari <?php echo $total_rows;?></strong> </div>
								</div>
							</form>
						</div>
						<div id="content-area">
							<ul id="myList">		
								<!--Job Row-->         
								<?php if($result){
									$CI =& get_instance();
									foreach($result as $row){
										$company_logo = ($row->company_logo)?$row->company_logo:'no_logo.jpg';
										$is_already_applied = $CI->is_already_applied_for_job($this->session->userdata('user_id'), $row->ID);		
										?>
										<li>
											<div class="box">
												<div class="thumb">
													<a href="<?php echo base_url('jobs/'.$row->job_slug);?>" class="thumbnail" title="<?php echo $row->job_title;?>"><img src="<?php echo base_url('public/uploads/employer/thumb/'.$company_logo);?>" alt="<?php echo base_url('company/'.$row->company_slug);?>" />
													</a>
												</div>
												<div class="text-col">
													<div class="hold"> 
														<h4><a href="<?php echo base_url('jobs/'.$row->job_slug);?>" class="jobtitle" title="<?php echo $row->job_title;?>"><?php echo word_limiter(strip_tags(str_replace('-',' ',$row->job_title)),7);?></a></h4>
														<p><?php echo word_limiter(strip_tags(str_replace('-',' ',$row->job_description)),22);?></p>
														<a href="#" class="text"><i class="fa fa-map-marker"></i> <?php echo $row->city;?></a>
														<a href="#" class="text"><i class="fa fa-calendar"></i> <?php echo date_formats($row->dated, 'M d, Y');?></a>
													</div>
												</div> 
												<?php
												if($is_already_applied=='yes'):
													?>
													<a href="javascript:;" class="applybtngray btn-1 btn-color-2 ripple">Sudah dilamar</a>
												<?php endif;?>


											</div>
										</li>
										<?php 
									}
								}else{ ?>
								<div class="err" align="center">
									<p><strong> <?php echo ($param=='')?'Please enter keywords above to display the relevant opened jobs.':'Sorry, no record found';?> </strong></p>
								</div>
								<?php }?>
							</ul>
						</div>

      <!--Pagination>
      
      	<div class="paginationWrap"> <?php #echo ($result)?$links:'';?> </div-->
      	</div>
      	<div class="col-md-4 col-sm-4">

      		<h2>Featured Jobs</h2>

      		<aside>

      			<div class="sidebar">

      				<div class="box">

      					<div class="thumb"> <a href="#"><img src="images/features-img-1.jpg" alt="img"></a>

      						<div class="caption"><img src="images/envato-text.png" alt="envato"></div>

      					</div>

      					<div class="text-box"> <a href="#" class="btn-time">Part Time</a>

      						<h4><a href="#">UI/UX Visual Designers</a></h4>

      						<a href="#" class="text"><i class="fa fa-map-marker"></i>Moorgate, London</a> <a href="#" class="text"><i class="fa fa-calendar"></i>Dec 22, 2015 - Mar 17, 2016 </a> <strong class="price"><i class="fa fa-money"></i>$4000 - $5000</strong> <a href="#" class="btn-apply">Apply for this Job</a> </div>

      					</div>

      					<h2>Jobs Gallery</h2>

      					<div class="sidebar-jobs">

      						<ul>

      							<li> <a href="#">General Compliance Officer</a> <span><i class="fa fa-map-marker"></i>Arlington, VA </span> <span><i class="fa fa-calendar"></i>Dec 22, 2015 - Mar 17, 2016 </span> </li>

      							<li> <a href="#">Medical Transcrption</a> <span><i class="fa fa-map-marker"></i>San Francisco, CA</span> <span><i class="fa fa-calendar"></i>Dec 22, 2015 - Mar 17, 2016 </span> </li>

      							<li> <a href="#">Support Coordinator</a> <span><i class="fa fa-map-marker"></i>Moorgate, London</span> <span><i class="fa fa-calendar"></i>Dec 22, 2015 - Mar 17, 2016 </span> </li>

      							<li> <a href="#">Mobile Application Developer</a> <span><i class="fa fa-map-marker"></i>New York, NY</span> <span><i class="fa fa-calendar"></i>Dec 22, 2015 - Mar 17, 2016 </span> </li>

      							<li> <a href="#">2D/3D Graphic Designer</a> <span><i class="fa fa-map-marker"></i>Moorgate, London</span> <span><i class="fa fa-calendar"></i>Dec 22, 2015 - Mar 17, 2016 </span> </li>

      							<li> <a href="#">Corporate Sales Executive</a> <span><i class="fa fa-map-marker"></i>Washington, DC </span> <span><i class="fa fa-calendar"></i>Dec 22, 2015 - Mar 17, 2016 </span> </li>

      							<li> <a href="#">International Tour Consultant</a> <span><i class="fa fa-map-marker"></i>Nationwide Anywhere </span> <span><i class="fa fa-calendar"></i>Dec 22, 2015 - Mar 17, 2016 </span> </li>

      							<li> <a href="#">Marketing Strategist</a> <span><i class="fa fa-map-marker"></i>Moorgate, London</span> <span><i class="fa fa-calendar"></i>Dec 22, 2015 - Mar 17, 2016 </span> </li>

      						</ul>

      					</div>

      				</div>

      			</aside>

      		</div>

      	</div>
      	<?php #$this->load->view('common/right_ads');?>
      </div>
  </div>
</section>
<!--/Latest Jobs Block-->
<!--Footer-->
<?php $this->load->view('common/footer'); ?>
<?php $this->load->view('common/before_body_close'); ?>
</body>
</html>