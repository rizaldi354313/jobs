
<!-- Bootstrap -->
<link href="<?php echo base_url('public/assets/css/custom.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('public/assets/css/bootstrap.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('public/assets/css/color.css'); ?>" rel="stylesheet" type="text/css">
<link href="<?php echo base_url('public/assets/css/responsive.css'); ?>" rel="stylesheet" type="text/css">
<link href="<?php echo base_url('public/assets/css/owl.carousel.css');?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('public/assets/css/font-awesome.min.css');?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('public/assets/css/jquery.mCustomScrollbar.css');?>" rel="stylesheet" type="text/css" />
<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,300italic,500,700,900' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="<?php echo base_url('public/css/w3.css') ?>">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="<?php //echo base_url('public/js/html5shiv.js');?>"></script>
      <script src="<?php //echo base_url('public/js/respond.min.js');?>"></script>
    <![endif]-->
<link rel="shortcut icon" href="<?php echo base_url('public/images/favicon.ico'); ?>">
<script>
var baseUrl = '<?php echo base_url();?>';
</script>
