<header id="header">
    <div class="cp-burger-nav"> 

      <!--BURGER NAV BUTTON-->

      <div id="cp_side-menu-btn" class="cp_side-menu"><a href="#" class=""><img src="<?php echo base_url(); ?>public/assets/images/menu-icon.png" alt="img"></a></div>

      <!--BURGER NAV BUTTON--> 

      

      <!--SIDEBAR MENU START-->

      <div id="cp_side-menu"> <a href="#" id="cp-close-btn" class="crose"><i class="fa fa-close"></i></a>

        <div class="cp-top-bar">

          <h4>For any Queries: +800 123 4567</h4>

          <div class="login-section"> <a href="login.html" class="btn-login">Log in</a> <a href="signup.html" class="btn-login">Signup</a> </div>

        </div>

        <strong class="logo-2"><a href="index.html"><img src="<?php echo base_url(); ?>public/assets/images/sidebar-logo.png" alt="img"></a></strong>

        <div class="content mCustomScrollbar">

          <div id="content-1" class="content">

            <div class="cp_side-navigation">

              <nav>

                <ul class="navbar-nav">

                  <li ><a href="<?php echo base_url();?>" >Home</a></li>

                  <li class="dropdown"><a href="#"  class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Jobseeker<span class="caret"></span></a>

                    <ul class="dropdown-menu" role="menu">

                      <li><a href="candidates-listings.html">Jobseeker Listing</a></li>

                      <li><a href="candidate-details.html">Jobseeker Details</a></li>

                      <li><a href="job-seekers.html">Jobseeker From</a></li>

                    </ul>

                  </li>

                  <li><a href="employers.html">Employers</a></li>

                  <li class="dropdown"><a href="#"  class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Jobs<span class="caret"></span></a>

                    <ul class="dropdown-menu" role="menu">

                      <li><a href="jobs.html">Latest Jobs</a></li>

                      <li><a href="jobs-detail.html">Jobs Details</a></li>

                      <li><a href="job-seekers.html">Jobs Form</a></li>

                    </ul>

                  </li>

                  <li class="dropdown"><a href="#"  class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Companies<span class="caret"></span></a>

                    <ul class="dropdown-menu" role="menu">

                      <li><a href="companies.html">Companies</a></li>

                      <li><a href="company-detail.html">Company Details</a></li>

                    </ul>

                  </li>

                  <li class="dropdown"><a href="#">Categories</a></li>

                  <li class="dropdown"><a href="#"  class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Blogs<span class="caret"></span></a>

                    <ul class="dropdown-menu" role="menu">

                      <li><a tabindex="-1" href="#">Post a Job</a></li>

                      <li><a tabindex="-1" href="companies.html">Companies</a></li>

                      <li><a tabindex="-1" href="#">Resumes</a></li>

                    </ul>

                  </li>

                  <li><a href="#">Features</a> </li>

                  <li><a href="<?php echo base_url("contact-us");?>">Contact</a> </li>

                </ul>

              </nav>

            </div>

          </div>

        </div>

        <div class="cp-sidebar-social">

          <ul>

            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>

            <li><a href="#"><i class="fa fa-twitter"></i></a></li>

            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>

            <li><a href="#"><i class="fa fa-facebook-f"></i></a></li>

          </ul>

        </div>

        <strong class="copy">JobInn &copy; 2016, All Rights Reserved</strong> </div>

      <!--SIDEBAR MENU END--> 

      

    </div>

	
	<div class="container"> 

      <!--NAVIGATION START-->

      <div class="navigation-col">

        <nav class="navbar navbar-inverse">

          <div class="navbar-header">

            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>

            <strong class="logo"><a href="index.html"><img src="<?php echo base_url("public/assets"); ?>/images/logo.png" alt="logo"></a></strong> </div>

          <div id="navbar" class="collapse navbar-collapse">

            <ul class="nav navbar-nav" id="nav">
	
            <?php 
				if($this->session->userdata('is_employer')==TRUE): 
			?>
              <li <?php # echo active_link('');?>><a href="<?php echo base_url();?>"> Home</a></li>
              <li <?php # echo active_link('employer');?>><a href="<?php echo base_url('employer/dashboard');?>" title="My Dashboard">Dashboard</a> </li>
              <li <?php # echo active_link('search-resume');?>><a href="<?php echo base_url('search-resume');?>" title="Search Resume">Jobseeker</a></li>
              <li <?php # echo active_link('contact-us');?>><a href="<?php echo base_url('contact-us');?>" title="Contact Us">Contact Us</a></li>
              <?php elseif($this->session->userdata('is_job_seeker')==TRUE):?>
              <li <?php # echo active_link('');?>><a href="<?php echo base_url();?>"> Home</a></li>
              <li <?php # echo active_link('jobseeker');?>><a href="<?php echo base_url('jobseeker/dashboard');?>" title="My Dashboard">Dashboard</a> </li>
              <li <?php # echo active_link('search-jobs');?>><a href="<?php echo base_url('search-jobs');?>" title="Search Jobs">Jobs</a></li>
              <li <?php # echo active_link('contact-us');?>><a href="<?php echo base_url('contact-us');?>" title="Contact Us">Contact Us</a></li>
              <?php else:?>
              <li <?php # echo active_link('');?>><a href="<?php echo base_url();?>"> Home</a></li>
              <li <?php # echo active_link('search-jobs');?>><a href="<?php echo base_url('search-jobs');?>" title="Search Government jobs in USA">Jobs</a> </li>
              <li <?php # echo active_link('search-resume');?>><a href="<?php echo base_url('search-resume');?>" title="Search Resume">Jobseeker</a></li>
              <li <?php # echo active_link('about-us.html');?>><a href="<?php echo base_url('about-us.html');?>" title="USA jobs free website">About Us</a></li>
              <li <?php # echo active_link('contact-us');?>><a href="<?php echo base_url('contact-us');?>" title="Contact Us">Contact Us</a></li>
              <?php /* ?>
			  <li <?php echo active_link('indeed_jobs');?>><a href="<?php echo base_url('indeed_jobs');?>" title="Indeed Jobs">Indeed</a></li>
              <?php */ ?>
			  <?php endif;?>

            </ul>

          </div>

        </nav>

      </div>

      <!--NAVIGATION END--> 

    </div>
  <div class="user-option-col">
  <?php if($this->session->userdata('is_user_login')==TRUE){ ?> 
	
      <div class="thumb">

        <div class="dropdown">

          <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><img style="width:36px" src="<?php echo base_url('public/uploads/candidate/'.$this->session->userdata("user_pic"));?>"  /><!--img src="<?php #echo base_url("public/assets")."/"; ?>images/user-img.png" alt="img" --></button>

          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">

            <li><a href="<?php echo base_url('jobseeker/my_account');?>">Kelola Akun</a></li>

            <li><a href="<?php echo base_url('jobseeker/cv_manager');?>">Resume Saya</a></li>

            <li><a href="<?php echo base_url('jobseeker/my_jobs');?>">Lowongan Saya</a></li>

            <li><a href="<?php echo base_url('user/logout');?>">Log off</a></li>

          </ul>

        </div>

      </div>
	<?php }else{ ?>
      <div class="dropdown-box">

        <div class="dropdown">

          <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> <img src="<?php echo base_url("public/assets")."/"; ?>images/option-icon.png" alt="img"> </button>

          <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">

            <li> <a href="<?php echo base_url('jobseeker-signup');?>" class="login-round"><i class="fa fa-users"></i></a> <a href="<?php echo base_url('jobseeker-signup');?>" class="btn-login">Log in as Jobseeker</a> </li>

            <li> <a href="<?php echo base_url('employer-signup');?>" class="login-round"><i class="fa fa-building"></i></a> <a href="<?php echo base_url('employer-signup');?>" class="btn-login">Log in as Employers</a> </li>

          </ul>

        </div>

      </div>
	<?php } ?>
    </div>

	
</header>
