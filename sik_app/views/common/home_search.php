<?php if($this->session->userdata('is_user_login')!=TRUE): ?>

  <div class="banner-outer">

    <div id="banner" class="element"> 
	        <?php 
			$slider_db = $this->db->get_where("pp_sliders",array("sts"=>"active")); 
			if($slider_db->num_rows() > 0){
				foreach($slider_db->result() as $slidkey=>$slidval){
				$active = "";
				if($slidkey == 0) $active = "active";
		?>    
					<img src="<?php echo base_url($slidval->image_url); ?>" data-color="lightblue" alt="<?php echo $slidkey + 1; ?> Image">
		<?php
				}
			}
		?>
	</div>

    <div class="caption">

      <div class="holder">

        <h1>Search Online Jobs or Hire Employees!</h1>

		<?php echo form_open_multipart('job_search/search',array('name' => 'jsearch', 'id' => 'jsearch'));?>

          <div class="container">

            <div class="row">

              <div class="col-md-6 col-sm-6">

                <input type="text"  name="job_params" id="job_params"  placeholder="Enter Job Title, Skills or Company">

              </div>

              <div class="col-md-5 col-sm-5">

                    <select class="col-md-12 col-sm-12 col-xs-12" name="jcity" id="jcity" style="padding:0 20px">      	
						<option value="" selected>Pilih Kota Tujuan</option>
					<?php if($cities_res): foreach($cities_res as $cities):?>
						<option value="<?php echo $cities->city_name;?>"><?php echo $cities->city_name;?></option>
					<?php endforeach; endif;?>
					</select>

              </div>

              <!--div class="col-md-3 col-sm-3">

                <input type="text" placeholder="Category">

              </div -->

              <div class="col-md-1 col-sm-1">

                <button type="submit"><i class="fa fa-search"></i></button>

              </div>

            </div>

          </div>

      <?php echo form_close();?>

        <div class="banner-menu">

          <!--ul>

            <li><a href="#">San Francisco</a></li>

            <li><a href="#">Palo Alto</a></li>

            <li><a href="#">Mountain View</a></li>

            <li><a href="#">Sacramento</a></li>

            <li><a href="#">New York</a></li>

            <li><a href="#">United Kindom</a></li>

            <li><a href="#">Asia Pacific</a></li>

          </ul -->

        </div>

        <div class="btn-row"> <a href="job-seekers.html"><i class="fa fa-user"></i>I’m a Jobseeker</a> <a href="employers.html"><i class="fa fa-building-o"></i>I’m an Employer</a> </div>

      </div>

    </div>

    <div class="browse-job-section">

      <div class="container">

        <div class="holder"> <a href="jobs.html" class="btn-brows">Browse Jobs</a> <strong class="title">Finds Jobs in San Francisco, Palo Alto, Mountain View, Sacramento, New York, United Kindom, Asia Pacific</strong> </div>

      </div>

    </div>

  </div>


<?php else: 

if($this->session->userdata('is_employer')==TRUE): ?>
<div class="col-md-12">
  <div class="employersection">
    <div class="col-md-6 col-md-offset-3">
      <h1>Cari Resume</h1>
      <?php echo form_open_multipart('resume_search/search',array('name' => 'rsearch', 'id' => 'rsearch'));?>
      <div class="input-group">
        <input type="text" name="resume_params" class="form-control" id="resume_params" placeholder="Cari Resume dari Skill atau Job desc" />
        <span class="input-group-btn">
        <input type="submit" name="resume_submit" class="btn" id="resume_submit" value="Cari" />
        </span> </div>
      <?php echo form_close();?> </div>
    <div class="col-md-12">
      <h1>Perusahaan Baru </h1>
      <a href="<?php echo base_url('employer/post_new_job');?>" class="postjobbtn" title="USA jobs">Posting Lamaran</a>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
</div>
<?php else: ?>
<div class="col-md-12">
  <div class="candidatesection">
    <div class="row">
    
    <div class="col-md-8 col-md-offset-2">
      <h1>Cari Pekerjaan</h1>
      <?php echo form_open_multipart('job_search/search',array('name' => 'jsearch', 'id' => 'jsearch'));?>
      <div class="input-group">
        <input type="text" name="job_params" id="job_params" class="form-control" placeholder="Title Pekerjaan atau Skill" />
        <span class="input-group-btn">
        <button type="submit" name="job_submit" id="job_submit" class="btn" value="Find"><i class="fa fa-search"></i> Cari</button>
        </span> </div>
      <?php echo form_close();?> </div>
      
     <div class="col-md-12">
       <div class="employersection">
      <h3>Upload CV</h3>
      <input type="submit" value="Upload Resume" title="job search engine USA" class="postjobbtn" alt="job search engine USA" onClick="document.location='<?php echo base_url('login');?>'" />
      </div>
      <div class="clear"></div>
    </div> 
    </div>
    <div class="clear"></div>
  </div>
</div>
<?php endif;?>
<?php endif;?>
