 <footer id="footer">

    <div class="text-box box">

      <h4>About Jobs Alert</h4>

      <p>Person usually begins a job by becoming an employee, volunteering, or starting a business. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusant doloremque laudantium, totam rem aperiam eaque ipsa quae.</p>

    </div>

    <div class="box">

      <h4>Keywords by Role</h4>

      <ul>

        <li><a href="#">Administrator</a></li>

        <li><a href="#">Accounts Officer</a></li>

        <li><a href="#">PHP Developer</a></li>

        <li><a href="#">UI/UX Designer</a></li>

        <li><a href="#">Project Manager</a></li>

        <li><a href="#">Public Relation Officer</a></li>

      </ul>

    </div>

    <div class="box">

      <h4>Keywords by Skills</h4>

      <ul>

        <li><a href="#">Administrator</a></li>

        <li><a href="#">Accounts Officer</a></li>

        <li><a href="#">PHP Developer</a></li>

        <li><a href="#">UI/UX Designer</a></li>

        <li><a href="#">Project Manager</a></li>

        <li><a href="#">Public Relation Officer</a></li>

      </ul>

    </div>

    <div class="box">

      <h4>Keywords by Location</h4>

      <ul>

        <li><a href="#">Administrator</a></li>

        <li><a href="#">Accounts Officer</a></li>

        <li><a href="#">PHP Developer</a></li>

        <li><a href="#">UI/UX Designer</a></li>

        <li><a href="#">Project Manager</a></li>

        <li><a href="#">Public Relation Officer</a></li>

      </ul>

    </div>

    <div class="box">

      <h4>Keywords by Market</h4>

      <ul>

        <li><a href="#">Administrator</a></li>

        <li><a href="#">Accounts Officer</a></li>

        <li><a href="#">PHP Developer</a></li>

        <li><a href="#">UI/UX Designer</a></li>

        <li><a href="#">Project Manager</a></li>

        <li><a href="#">Public Relation Officer</a></li>

      </ul>

    </div>

    <form action="#">

      <h4>Subscribe for Latest Jobs Alerts</h4>

      <input type="text" placeholder="Name" required>

      <input type="text" placeholder="Email" required>

      <input type="submit" value="Subscribe Alerts">

    </form>

    <div class="container">

      <div class="bottom-row"> <strong class="copyrights">JobInn &copy; 2016, All Rights Reserved, Design &amp; Developed By: <a href="http://crunchpress.com/" target="_blank">CrunchPress</a></strong>

        <div class="footer-social">

          <ul>

            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>

            <li><a href="#"><i class="fa fa-twitter"></i></a></li>

            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>

            <li><a href="#"><i class="fa fa-facebook-f"></i></a></li>

          </ul>

        </div>

      </div>

    </div>

  </footer>
<?php /*
<div class="footerWrap">
  <div class="container">
    <div class="col-md-4">
      <img src="<?php echo base_url('public/images/logo.png');?>" alt="USA jobs website" />
      <br><br>
      <p>Gandaria 8 Office Tower 8th Floor, Jl. Sultan Iskandar Muda Kebayoran Lama, Jakarta Selatan 12240</p>
     <p>
		<br />
		<ul style="list-style:none;color:white;">
			<li>m&nbsp;&nbsp;: 0812.2005.7009</li>
			<li>p&nbsp;&nbsp;&nbsp;: 021 – 2985 1625</li>
			<li>f&nbsp;&nbsp;&nbsp;: 021 – 2985 1601</li>
			<li>e&nbsp;&nbsp;&nbsp;: Recruitment@Mitrasolusijakarta.com</li>
			<li>e&nbsp;&nbsp;&nbsp;: Contact@Mitrasolusijakarta.com</li>
		</ul>
	  </p>
      <!--Social>
        
        <div class="social">
        <a href="http://www.twitter.com" target="_blank"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
        <a href="http://www.plus.google.com" target="_blank"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a>
        <a href="http://www.facebook.com" target="_blank"> <i class="fa fa-facebook-square" aria-hidden="true"></i></a>
        <a href="https://www.pinterest.com" target="_blank"><i class="fa fa-pinterest-square" aria-hidden="true"></i></a>
        <a href="https://www.youtube.com" target="_blank"><i class="fa fa-youtube-square" aria-hidden="true"></i></a>
        <a href="https://www.linkedin.com" target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
        </div-->
        
      
    </div>
    <div class="col-md-3 text-left">
      <h4>Our Office</h4>
      <iframe width="100%" height="250" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJGRaRaBfxaS4Rox2rbssN2HA&key=AIzaSyCJ1skasCWNdj2uGSQuV-wt-RF4Ni2oMdk" allowfullscreen></iframe>
    </div>
     <div class="col-md-2 text-left">
      <h4>Article</h4>
      <ul class="quicklinks">
        <li><a href="<?php echo base_url('how-to-get-job.html');?>" title="How to get Job">How to get Job</a></li>
        <li><a href="<?php echo base_url('search-jobs');?>" title="New Job Openings">New Job Openings</a></li>
        <li><a href="<?php echo base_url('search-resume');?>" title="New Job Openings">Resume Search</a></li>
        <li><a href="<?php echo base_url('interview.html');?>" title="Preparing for Interview">Preparing for Interview</a> </li>
        <li><a href="<?php echo base_url('how-to-get-job.html');?>" title="How to get Job">How to get Job</a></li>
        <li><a href="<?php echo base_url('cv-writing.html');?>" title="CV Writing">CV Writing</a></li>
        <li><a href="<?php echo base_url('interview.html');?>" title="Preparing for Interview">Preparing for Interview</a></li>
      </ul>
    </div>
    
    <div class="col-md-3 text-left">
      <h4>Category Opportunities</h4>
      <div class="col-md-6" style="padding:0px">
		<ul class="quicklinks">
		  <?php
			$ifx = 0;
			$res_inds = $this->industries_model->get_top_industries();
			if($res_inds):
			
				foreach($res_inds as $row_inds):
				
		?>
        <li><a href="<?php echo base_url('industry/'.$row_inds->slug);?>" title="<?php echo $row_inds->industry_name;?> Jobs"><?php echo $row_inds->industry_name;?> Jobs</a></li>
        <?php 
				if(!empty($ifx) && $ifx % 5 == 0){
					echo '</ul></div><div class="col-md-6" style="padding:0px"><ul class="quicklinks">';
				}
				$ifx++;
				
				endforeach;
				
			endif;

		  ?>
		</ul>
	  </div>
    </div>
	<?php /*
    <div class="col-md-4">
      <h5>Kota - Kota Populer</h5>
      <ul class="citiesList">
        <li><a href="<?php echo base_url('search/new-york');?>" title="Jobs in New York">New York</a></li>
        <li><a href="<?php echo base_url('search/los-angeles');?>" title="Jobs in 	Los Angeles">Los Angeles</a></li>
        <li><a href="<?php echo base_url('search/chicago');?>" title="Jobs in Chicago">Chicago</a></li>
        <li><a href="<?php echo base_url('search/houston');?>" title="Jobs in Houston">Houston</a></li>
        <li><a href="<?php echo base_url('search/san-diego');?>" title="Jobs in San Diego">San Diego</a></li>
        <li><a href="<?php echo base_url('search/san-jose');?>" title="Jobs in San Jose">San Jose</a></li>
        <li><a href="<?php echo base_url('search/austin');?>" title="Jobs in Austin">Austin</a></li>
        <li><a href="<?php echo base_url('search/san-francisco');?>" title="Jobs in San Francisco">	San Francisco</a></li>
        <li><a href="<?php echo base_url('search/columbus');?>" title="Jobs in Columbus">Columbus</a></li>
        <li><a href="<?php echo base_url('search/boston');?>" title="Jobs in Boston">Boston</a></li>
        <li><a href="<?php echo base_url('search/washington');?>" title="Jobs in Washington">Washington</a></li>
        <li><a href="<?php echo base_url('search/las-vegas');?>" title="Jobs in Las Vegas">Las Vegas</a></li>
      </ul>
      <div class="clear"></div>
    </div>
    <?php  ?>
    
  </div>
</div>
	<div class="clear"></div>
	<div id="copyright">
      <div class="bttxt">Copyright &copy; <?php echo date('Y');?>, Mitra Solusi Jakarta.</div>
    </div>
</div>
<?php #echo $ads_row->google_analytics; */  ?> 