<script>
var slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
  showDivs(slideIndex += n);
}

function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length}
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}
</script><!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="<?php echo base_url('public/assets/js/jquery-1.11.3.min.js');?>"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="<?php echo base_url('public/assets/js/bootstrap.min.js');?>"></script>
<script src="<?php echo base_url('public/assets/js/owl.carousel.min.js');?>"></script>
<!-- script src="<?php echo base_url('public/assets/js/velocity.min.js');?>"></script -->
<script src="<?php echo base_url('public/assets/js/jquery.kenburnsy.js');?>"></script>
<script src="<?php echo base_url('public/assets/js/jquery.velocity.min.js');?>"></script>
<!-- FlexSlider  --> 
<script src="<?php echo base_url('public/js/jquery.flexslider.js');?>" type="text/javascript"></script> 
<script>
// Can also be used with $(document).ready()
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    animationLoop: false,
    itemWidth: 250,
    minItems: 1,
    maxItems: 1
  });
});
</script>

<script src="<?php echo base_url('public/assets/js/jquery.mCustomScrollbar.concat.min.js');?>"></script>
<script src="<?php echo base_url('public/assets/js/form.js');?>"></script>
<script src="<?php echo base_url('public/assets/js/custom.js');?>"></script>
<script src="<?php echo base_url('public/js/functions.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('public/js/validation.js');?>" type="text/javascript"></script>





