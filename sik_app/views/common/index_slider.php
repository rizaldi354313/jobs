      <div id="mycarousel" class="carousel slide" data-ride="carousel">
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
        <?php 
			$slider_db = $this->db->get_where("pp_sliders",array("sts"=>"active")); 
			if($slider_db->num_rows() > 0){
				foreach($slider_db->result() as $slidkey=>$slidval){
				$active = "";
				if($slidkey == 0) $active = "active";
		?>    
				<div class="item <?php echo $active; ?>">
					<img src="<?php echo base_url($slidval->image_url); ?>" data-color="lightblue" alt="<?php echo $slidkey + 1; ?> Image">
					<!-- /header-text -->
				</div>
		<?php
				}
			}
		?>

        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#mycarousel" role="button" data-slide="prev">
            <span class="fa fa-chevron-left" aria-hidden="true" style=""></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#mycarousel" role="button" data-slide="next">
            <span class="fa fa-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>


<?php 
/*
else: 

if($this->session->userdata('is_employer')==TRUE): ?>
<div class="col-md-12">
  <div class="employersection">
    <div class="col-md-6 col-md-offset-3">
      <h1>Cari Resume</h1>
      <?php echo form_open_multipart('resume_search/search',array('name' => 'rsearch', 'id' => 'rsearch'));?>
      <div class="input-group">
        <input type="text" name="resume_params" class="form-control" id="resume_params" placeholder="Cari Resume dari Skill atau Job desc" />
        <span class="input-group-btn">
        <input type="submit" name="resume_submit" class="btn" id="resume_submit" value="Cari" />
        </span> </div>
      <?php echo form_close();?> </div>
    <div class="col-md-12">
      <h1>Perusahaan Baru </h1>
      <a href="<?php echo base_url('employer/post_new_job');?>" class="postjobbtn" title="USA jobs">Posting Lamaran</a>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
</div>
<?php else: ?>
<div class="col-md-12">
  <div class="candidatesection">
    <div class="row">
    
    <div class="col-md-8 col-md-offset-2">
      <h1>Search Job</h1>
      <?php echo form_open_multipart('job_search/search',array('name' => 'jsearch', 'id' => 'jsearch'));?>
      <div class="input-group">
        <input type="text" name="job_params" id="job_params" class="form-control" placeholder="Title Pekerjaan atau Skill" />
        <span class="input-group-btn">
        <button type="submit" name="job_submit" id="job_submit" class="btn" value="Find"><i class="fa fa-search"></i> Search</button>
        </span> </div>
      <?php echo form_close();?> </div>
      
     <div class="col-md-12">
       <div class="employersection">
      <h3>Upload CV</h3>
      <input type="submit" value="Upload Resume" title="job search engine USA" class="postjobbtn" alt="job search engine USA" onClick="document.location='<?php echo base_url('login');?>'" />
      </div>
      <div class="clear"></div>
    </div> 
    </div>
    <div class="clear"></div>
  </div>
</div>
<?php endif; */
?>
