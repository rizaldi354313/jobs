<!DOCTYPE html>
<html lang="en">
<head>
<?php $this->load->view('common/meta_tags'); ?>
<?php $this->load->view('common/before_head_close'); ?>
</head>
<body class="theme-style-1">
<?php $this->load->view('common/after_body_open'); ?>
<div class="wrapper">
<!--Header-->
<?php $this->load->view('common/header'); ?>
<!--/Header--> 
<section id="inner-banner">

    <div class="container">

		<h1 ><?php echo $row->pageTitle;?></h1>
			
    </div>

</section>

<!--Detail Info-->
<div id="main">
<section class="resumes-section padd-tb">
  <div class="container">
      <div class="row">
		<div class="col-md-12"><!--Job Detail-->
	    <div class="resumes-content">
			<div class="box">
			<!--Job Description-->
			<div class="companydescription">
			  <div class="row">
				<div class="col-md-12">
				  <p><?php echo $row->pageContent;?></p>
				</div>
			  </div>
			</div>
		  </div>
		</div>
		</div>
	  </div>
  </div>
</section>
</div>
<!--Footer-->
<?php $this->load->view('common/footer'); ?>
<?php $this->load->view('common/before_body_close'); ?>
</body>
</html>