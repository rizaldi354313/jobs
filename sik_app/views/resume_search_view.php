<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view('common/meta_tags'); ?>
  <meta name="keywords" content="<?php echo $param;?> Jobs" />
  <meta name="description" content="<?php echo $param;?> Jobs ,Find best Jobs. Jobs at <?php echo SITE_NAME;?>." />
  <title><?php echo $title;?></title>
  <?php $this->load->view('common/before_head_close'); ?>
</head>
<body class="theme-style-1">
  <?php $this->load->view('common/after_body_open'); ?>
  <div class="wrapper">
    <!--Header-->
    <?php $this->load->view('common/header'); ?>
    <!--/Header--> 
    <section id="inner-banner">

      <div class="container">

        <h1>Find Your Employee</h1>

      </div>

    </section>
    <section class="popular-job-caregries popular-categories candidates-listing">

      <div class="holder">

        <div class="container">

          <h4>Popular Job Categories</h4>

          <div id="popular-job-slider" class="owl-carousel owl-theme">

            <div class="item">

              <div class="box"> <img alt="img" src="<?php echo base_url('') ?>public/images/categories-icon-1.png">

                <h4><a href="#">Art, Design &amp; Multimedia</a></h4>

                <strong>6,213 Jobs</strong>

                <p>Available in Design &amp; Multimedia</p>

              </div>

            </div>

            <div class="item">

              <div class="box"> <img alt="img" src="<?php echo base_url('') ?>public/images/categories-icon-2.png">

                <h4><a href="#">Restaurant, Food, Hotels</a></h4>

                <strong>3,750 Jobs</strong>

                <p>Available in Restaurant &amp; Hotels</p>

              </div>

            </div>

            <div class="item">

              <div class="box"> <img alt="img" src="<?php echo base_url('') ?>public/images/categories-icon-3.png">

                <h4><a href="#">Transporation</a></h4>

                <strong>1,265 Jobs</strong>

                <p>Available in Transportation</p>

              </div>

            </div>

            <div class="item">

              <div class="box"> <img alt="img" src="<?php echo base_url('') ?>public/images/categories-icon-4.png">

                <h4><a href="#">Healthcare &amp; Medicine</a></h4>

                <strong>5,913 Jobs</strong>

                <p>Available in Medical</p>

              </div>

            </div>

            <div class="item">

              <div class="box"> <img alt="img" src="<?php echo base_url('') ?>public/images/categories-icon-5.png">

                <h4><a href="#">Software &amp; Development</a></h4>

                <strong>7,418 Jobs</strong>

                <p>Available in IT</p>

              </div>

            </div>

            <div class="item">

              <div class="box"> <img alt="img" src="<?php echo base_url('') ?>public/images/categories-icon-6.png">

                <h4><a href="#">Accounting &amp; Finance</a></h4>

                <strong>8,045 Jobs</strong>

                <p>Available in Accounts &amp; Finance</p>

              </div>

            </div>

            <div class="item">

              <div class="box"> <img alt="img" src="<?php echo base_url('') ?>public/images/categories-icon-7.png">

                <h4><a href="#">Education &amp; Academia</a></h4>

                <strong>3,891 Jobs</strong>

                <p>Available in Education</p>

              </div>

            </div>

            <div class="item">

              <div class="box"> <img alt="img" src="<?php echo base_url('') ?>public/images/categories-icon-8.png">

                <h4><a href="#">Construction, Engineering</a></h4>

                <strong>9,942 Jobs</strong>

                <p>Available in Architect</p>

              </div>

            </div>

          </div>

        </div>

      </div>

    </section>
    <!--Search Block-->
    <section class="candidates-search-bar">
      <div class="container">
       <?php echo form_open_multipart('resume_search/search',array('name' => 'rsearch', 'id' => 'rsearch'));?>
       <div class="row">
        <div class="col-md-12">
          <div class="employersection">

            <div class="col-md-10 col-sm-9">                        
              <input type="text" name="resume_params" id="resume_params" class="form-control" placeholder="Skill or Job Title" value="<?php echo $param;?>" />            
            </div>
            <div class="col-md-2 col-sm-3">
              <button type="submit" name="resume_submit" class="btn btn-primary" id="resume_submit" ><i class="fa fa-search"></i></button>
            </div>

            
            <div class="clear"></div>
          </div>
        </div>
        <div class="clear"></div>
      </div>
      <?php echo form_close();?> 		
    </div>
  </section>
  <!--/Search Block--> 
  <!--Latest Jobs Block-->
  <div class="main">
    <section class="resumes-section padd-tb">
      <div class="container">
        <div class="row"> 

          <!--Left Col-->
          <div class="col-md-9 col-sm-8">
            <!--Mid Col-->
            <div class="check-filter">

              <form action="#">

                <ul>

                  <li>

                    <input id="id1" type="checkbox" />

                    <label>All Jobs</label>

                  </li>

                  <li>

                    <input id="id2" type="checkbox" />

                    <label>Part Time</label>

                  </li>

                  <li>

                    <input id="id3" type="checkbox" />

                    <label>Full Time</label>

                  </li>

                  <li>

                    <input id="id4" type="checkbox" />

                    <label>Freelance</label>

                  </li>

                  <li>

                    <input id="id5" type="checkbox" />

                    <label>Contract</label>

                  </li>

                  <li>

                    <input id="id6" type="checkbox" />

                    <label>Internship</label>

                  </li>

                </ul>

              </form>

            </div>


            <div class="searchjoblist col-md-<?php echo ($result)?'12':'12';?>"> 

              <!--Jobs List-->

              <div class="boxwraper">
                <div class="titlebar">
                  <div class="row">
                    <div class="col-md-6"><h2><b><?php echo $param;?> Resume</b> Karyawan</h2></div>
                    <div class="col-md-6 text-right"><strong>Resume <?php echo $from_record.' - '.$page;?> dari <?php echo ($total_rows>20)?'20':$total_rows;?></strong> </div>
                  </div>
                </div>
                <div id="content-area" class="row searchlist"> 

                  <!--Job Row-->

                  <?php 
                  if($result):
                   foreach($result as $row):
                    $candidate_logo = ($row->photo)?$row->photo:'no_pic.jpg';
                    $age = date_difference_in_years($row->dob, date("Y-m-d"));
                    $encrypt_id = $this->custom_encryption->encrypt_data($row->ID);
                    $row_latest_exp = $this->jobseeker_experience_model->get_latest_job_by_seeker_id($row->ID);

                    $lastest_job_title = ($row_latest_exp)?word_limiter(strip_tags(ucwords($row_latest_exp->job_title)),15):'';
                    $edu_row = $this->jobseeker_academic_model->get_record_by_seeker_id($row->ID);

                    $latest_education = ($edu_row)?$edu_row->degree_title.' - '.$edu_row->institude.', '.$edu_row->city:'';
                    $latest_education = trim(ucwords($latest_education),', ');

                    $total_experience = $this->jobseeker_experience_model->get_total_experience_by_seeker_id($row->ID);
                    $total_experience = number_format($total_experience,'1','.','');
                    $total_experience = ($total_experience>0)?$total_experience.' tahun':'';
                    $final_exp ='';
                    $total_experience_array = explode('.',$total_experience);

                    if(count($total_experience_array)>1){

                     $year = ($total_experience_array[0]>0)?$total_experience_array[0]:'';
                     $year = $year.' '.get_singular_plural($year, 'Tahun', 'Tahun');

                     $monthval = substr($total_experience_array[1],0,1);
                     $month = ($monthval>0)?$monthval:'';
                     $month = $month.' '.get_singular_plural($month, 'Bulan', 'Bulan');

                     $final_exp = (trim($year)!='' && trim($month)!='')?$year.' dan '.$month:$year.' '.$month;
                     $final_exp = trim($final_exp);
                   }
                   else{
                     $final_exp ='Tidak ada pengalaman';	
                   }

                   $keywords_array = explode(', ',@$row->keywords);
                   ?>
                   <div id="myList" class="col-md-12">
                    <div class="box">
                      <div class="col-md-2"><div class="frame"><a href="<?php echo base_url('candidate/'.$encrypt_id);?>" target="_blank" class="thumbnail"><img width="165" height="165" src="<?php echo base_url('public/uploads/candidate/thumb/'.$candidate_logo);?>" alt="<?php echo $row->first_name;?>" style="max-height:80px;" /></a></div></div>
                      <div class="col-md-10">
                        <div class="col-md-10">
                          <div> 
                            <h2><a href="<?php echo base_url('candidate/'.$encrypt_id);?>" target="_blank" class="devtitle"><?php echo word_limiter(strip_tags($row->first_name),7);?></a></h2> 
                            <div class="devinfo"><h4><?php echo $lastest_job_title;?></h4></div>
                            <span class="aboutloc">[ <?php echo ucwords($row->gender);?>, <?php echo $age;?>, <?php echo ucwords($row->city);?> ]</span> </div>
                          <div class="devexp"><?php echo $final_exp;?></div>
                          <div class="devedu"><?php echo $latest_education;?></div>
                          <?php if($keywords_array):?>
                            <div class="devinfo"><strong>Skills:</strong>
                              <?php 
                              $i=0;
                              foreach($this->jobseeker_skills_model->get_records_by_seeker_id($row->ID) as $keyword_row):
                                $i++;
                                if($i<5):
                                  ?>
                                  <a href="<?php echo base_url('search-resume/'.make_slug($keyword_row->skill_name));?>" class="keyword" target="_blank"><?php echo $keyword_row->skill_name;?></a>
                                <?php endif; endforeach;?>
                              </div>
                            <?php endif;?>
                          </div>
                          <div class="col-md-2"> <a href="<?php echo base_url('candidate/'.$encrypt_id);?>" target="_blank" class="btn-style-1">Lihat Profil</a>
                            <div class="date"></div>
                          </div>
                          <div class="clear"> </div>
                        </div>
                        <div class="clear"></div>
                      </div>
                    </div>
                    <?php 
                  endforeach;
                  else: ?>
                  <div class="err" align="center">
                    <p><strong> <?php echo ($param=='')?'Please enter keywords above to display the relevant opened jobs.':'Sorry, no record found';?> </strong></p>
                  </div>
                <?php endif;?>
              </div>
            </div>

            <!--Pagination-->

            <div class="paginationWrap">
              <?php //echo ($result)?$links:'';?>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-4">

          <h2>Related People</h2>

          <aside>

            <div class="sidebar">

              <div class="related-people">

                <ul>

                  <li>

                    <div class="frame"><a href="#"><img src="<?php echo base_url('') ?>public/images/resumes/related-img-1.jpg" alt="img"></a></div>

                    <div class="text-box"> <strong class="name"><a href="#">Bettymesfin</a></strong> <span><i class="fa fa-tags"></i>Developer</span> <span><i class="fa fa-map-marker"></i>Atlanta</span> </div>

                  </li>

                  <li>

                    <div class="frame"><a href="#"><img src="<?php echo base_url('') ?>public/images/resumes/related-img-2.jpg" alt="img"></a></div>

                    <div class="text-box"> <strong class="name"><a href="#">Cammy</a></strong> <span><i class="fa fa-tags"></i>Front Desk Specialist</span> <span><i class="fa fa-map-marker"></i>Berlin</span> </div>

                  </li>

                  <li>

                    <div class="frame"><a href="#"><img src="<?php echo base_url('') ?>public/images/resumes/related-img-3.jpg" alt="img"></a></div>

                    <div class="text-box"> <strong class="name"><a href="#">Jane Doe</a></strong> <span><i class="fa fa-tags"></i>Marketing Online</span> <span><i class="fa fa-map-marker"></i>Atlanta</span> </div>

                  </li>

                  <li>

                    <div class="frame"><a href="#"><img src="<?php echo base_url('') ?>public/images/resumes/related-img-4.jpg" alt="img"></a></div>

                    <div class="text-box"> <strong class="name"><a href="#">Angela Haze</a></strong> <span><i class="fa fa-tags"></i>Accountant</span> <span><i class="fa fa-map-marker"></i>Berlin</span> </div>

                  </li>

                  <li>

                    <div class="frame"><a href="#"><img src="<?php echo base_url('') ?>public/images/resumes/related-img-5.jpg" alt="img"></a></div>

                    <div class="text-box"> <strong class="name"><a href="#">Suzy Biggs</a></strong> <span><i class="fa fa-tags"></i>HR Manager Expert</span> <span><i class="fa fa-map-marker"></i>London, UK</span> </div>

                  </li>

                  <li>

                    <div class="frame"><a href="#"><img src="<?php echo base_url('') ?>public/images/resumes/related-img-6.jpg" alt="img"></a></div>

                    <div class="text-box"> <strong class="name"><a href="#">Rachael Smith</a></strong> <span><i class="fa fa-tags"></i>Lead Designer</span> <span><i class="fa fa-map-marker"></i>New York, NY</span> </div>

                  </li>

                  <li>

                    <div class="frame"><a href="#"><img src="<?php echo base_url('') ?>public/images/resumes/related-img-1.jpg" alt="img"></a></div>

                    <div class="text-box"> <strong class="name"><a href="#">Bettymesfin</a></strong> <span><i class="fa fa-tags"></i>Developer</span> <span><i class="fa fa-map-marker"></i>Atlanta</span> </div>

                  </li>

                  <li>

                    <div class="frame"><a href="#"><img src="<?php echo base_url('') ?>public/images/resumes/related-img-2.jpg" alt="img"></a></div>

                    <div class="text-box"> <strong class="name"><a href="#">Cammy</a></strong> <span><i class="fa fa-tags"></i>Front Desk Specialist</span> <span><i class="fa fa-map-marker"></i>Berlin</span> </div>

                  </li>

                  <li>

                    <div class="frame"><a href="#"><img src="<?php echo base_url('') ?>public/images/resumes/related-img-3.jpg" alt="img"></a></div>

                    <div class="text-box"> <strong class="name"><a href="#">Jane Doe</a></strong> <span><i class="fa fa-tags"></i>Marketing Online</span> <span><i class="fa fa-map-marker"></i>Atlanta</span> </div>

                  </li>

                  <li>

                    <div class="frame"><a href="#"><img src="<?php echo base_url('') ?>public/images/resumes/related-img-4.jpg" alt="img"></a></div>

                    <div class="text-box"> <strong class="name"><a href="#">Angela Haze</a></strong> <span><i class="fa fa-tags"></i>Accountant</span> <span><i class="fa fa-map-marker"></i>Berlin</span> </div>

                  </li>

                  <li>

                    <div class="frame"><a href="#"><img src="<?php echo base_url('') ?>public/images/resumes/related-img-5.jpg" alt="img"></a></div>

                    <div class="text-box"> <strong class="name"><a href="#">Suzy Biggs</a></strong> <span><i class="fa fa-tags"></i>HR Manager Expert</span> <span><i class="fa fa-map-marker"></i>London, UK</span> </div>

                  </li>

                  <li>

                    <div class="frame"><a href="#"><img src="<?php echo base_url('') ?>public/images/resumes/related-img-6.jpg" alt="img"></a></div>

                    <div class="text-box"> <strong class="name"><a href="#">Rachael Smith</a></strong> <span><i class="fa fa-tags"></i>Lead Designer</span> <span><i class="fa fa-map-marker"></i>New York, NY</span> </div>

                  </li>

                </ul>

              </div>

            </div>

          </aside>

        </div>

      </div>
    </div>
  </div>
</section>
</div>
<!--/Latest Jobs Block-->
<!--Footer-->
<?php $this->load->view('common/footer'); ?>
<?php $this->load->view('common/before_body_close'); ?>
</body>
</html>