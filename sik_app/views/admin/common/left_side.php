<aside class="left-side sidebar-offcanvas"> 
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar"> 
    <!-- Sidebar user panel -->
    <ul class="sidebar-menu">
      <li> <a href="<?php echo base_url('admin/dashboard');?>"><i class="fa fa-dashboard"></i> <span>Dashboard</span> </a> </li>
      <li class="<?php if($this->uri->segment(2)&& $this->uri->segment(2) == "employers") echo "active"; ?>"> <a href="<?php echo base_url('admin/employers');?>"><i class="fa fa-bank"></i><span>Perusahaan</span> </a> </li>
      <li class="<?php if($this->uri->segment(2)&& $this->uri->segment(2) == "job_seekers") echo "active"; ?>"> <a href="<?php echo base_url('admin/job_seekers');?>"><i class="fa fa-users"></i><span>Kelola Pencari Kerja</span> </a> </li>
      <li class="<?php if($this->uri->segment(2)&& $this->uri->segment(2) == "posted_jobs") echo "active"; ?>"> <a href="<?php echo base_url('admin/posted_jobs');?>"><i class="fa fa-paper-plane"></i><span>Kelola Postingan Lowongan</span> </a> </li>
      <li class="<?php if($this->uri->segment(2)&& $this->uri->segment(2) == "pages") echo "active"; ?>"> <a href="<?php echo base_url('admin/pages');?>"><i class="fa fa-desktop"></i><span>CMS Website</span> </a> </li>
      <li class="<?php if($this->uri->segment(2)&& $this->uri->segment(2) == "invite_employer") echo "active"; ?>"> <a href="<?php echo base_url('admin/invite_employer');?>"><i class="fa fa-envelope"></i><span>Undang Perusahaan</span> </a> </li>
      <li class="<?php if($this->uri->segment(2)&& $this->uri->segment(2) == "invite_jobseeker") echo "active"; ?>"> <a href="<?php echo base_url('admin/invite_jobseeker');?>"><i class="fa fa-envelope-square"></i><span>Undang Pencari Kerja</span> </a> </li>
      <li class="<?php if($this->uri->segment(2)&& $this->uri->segment(2) == "industries") echo "active"; ?>"> <a href="<?php echo base_url('admin/industries');?>"><i class="fa fa-building"></i><span>Kelola Tipe Perusahaan</span> </a> </li>
      <li class="<?php if($this->uri->segment(2)&& $this->uri->segment(2) == "institute") echo "active"; ?>"> <a href="<?php echo base_url('admin/institute');?>"><i class="fa fa-graduation-cap"></i><span>Kelola Pendidikan</span> </a> </li>
      <li class="<?php if($this->uri->segment(2)&& $this->uri->segment(2) == "salary") echo "active"; ?>"> <a href="<?php echo base_url('admin/salary');?>"><i class="fa fa-money"></i><span>Kelola Gaji</span> </a> </li>
      <li class="<?php if($this->uri->segment(2)&& $this->uri->segment(2) == "qualification") echo "active"; ?>"> <a href="<?php echo base_url('admin/qualification');?>"><i class="fa fa-check-square"></i><span>Kelola Kualifikasi</span> </a> </li>
      <li class="<?php if($this->uri->segment(2)&& $this->uri->segment(2) == "ads") echo "active"; ?>"> <a href="<?php echo base_url('admin/ads');?>"><i class="fa fa-mobile"></i><span>Kelola Iklan</span> </a> </li>
      <li class="<?php if($this->uri->segment(2)&& $this->uri->segment(2) == "countries") echo "active"; ?>"> <a href="<?php echo base_url('admin/countries');?>"><i class="fa fa-globe"></i><span>Kelola Negara</span> </a> </li>
      <li class="<?php if($this->uri->segment(2)&& $this->uri->segment(2) == "cities") echo "active"; ?>"> <a href="<?php echo base_url('admin/cities');?>"><i class="fa fa-map-marker"></i><span>Kelola Kota</span> </a> </li>
      <li class="<?php if($this->uri->segment(2)&& $this->uri->segment(2) == "prohibited_keyword") echo "active"; ?>"> <a href="<?php echo base_url('admin/prohibited_keyword');?>"><i class="fa fa-key"></i><span>Kelola Kata Terlarang</span> </a> </li>
      <li class="<?php if($this->uri->segment(2)&& $this->uri->segment(2) == "skills") echo "active"; ?>"> <a href="<?php echo base_url('admin/skills');?>"><i class="fa fa-bar-chart-o"></i><span>Kelola Skill</span> </a> </li>
      <li> <a href="<?php echo base_url('admin/home/logout');?>"><i class="fa fa-sign-out"></i><span>Logout</span> </a> </li>
    </ul>
  </section>
  <!-- /.sidebar --> 
</aside>
