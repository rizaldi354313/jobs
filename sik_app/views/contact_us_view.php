
<!DOCTYPE html>
<html lang="en">
<head>
<?php $this->load->view('common/meta_tags'); ?>
<title><?php echo $title;?></title>
<?php $this->load->view('common/before_head_close'); ?>
</head>
<body class="theme-style-1">
<?php $this->load->view('common/after_body_open'); ?>
<div class="wrapper">
<!--Header-->
<?php $this->load->view('common/header'); ?>
<section id="inner-banner">

    <div class="container">

      <h1>Contact Us</h1>

    </div>

  </section>
<iframe width="100%" height="250" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJGRaRaBfxaS4Rox2rbssN2HA&key=AIzaSyCJ1skasCWNdj2uGSQuV-wt-RF4Ni2oMdk" allowfullscreen></iframe>
<div id="main">    
<!--/Header-->
 <section class="contact-section">
	<div class="map-box">
		<div class="contact-form padd-tb">
			<div class="container detailinfo">
			  <div class="row"> <?php echo form_open_multipart('contact_us',array('name' => 'frm_contact_us', 'id' => 'frm_contact_us', 'onSubmit' => 'return validate_contact_form(this);'));?>
				<div class="col-md-12">
                <h2>Get in Touch</h2>
				  <!--Account info-->
					<?php echo $this->session->flashdata('success_msg');?>
				  <!--Professional info-->
				  <br />
				  <div class="form-group">
					<div class="formint">
					<div class="row">
						<div class="col-md-4">
						  <div class="form-group <?php echo (form_error('full_name') || $msg)?'has-error':'';?>">
							<input type="text" class="form-control" name="full_name" placeholder="Full Name *" id="full_name" value="<?php echo set_value('full_name'); ?>" />
							<?php echo form_error('full_name'); ?>
						  </div>
						</div>
						<div class="col-md-4">
						  <div class="form-group <?php echo (form_error('email') || $msg)?'has-error':'';?>">
							<input type="text" class="form-control" placeholder="Email Address *" name="email" id="email" value="<?php echo set_value('email'); ?>" />
							<?php echo form_error('email'); ?>
						  </div>
						</div>
						<div class="col-md-4">  
						  <div class="form-group <?php echo (form_error('phone') || $msg)?'has-error':'';?>">
							<input type="text" class="form-control" placeholder="Phone Number *" name="phone" id="phone" value="<?php echo set_value('phone'); ?>" />
							<?php echo form_error('phone'); ?>
						  </div>
						</div>
					  </div>
					  
					  <div class="form-group <?php echo (form_error('phone') || $msg)?'has-error':'';?>">
						<input type="text" class="form-control" placeholder="City"  name="city" id="city" value="<?php echo set_value('city'); ?>" />
						<?php echo form_error('city'); ?>
					  </div>
					  
					  <div class="input-group <?php echo (form_error('message') || $msg)?'has-error':'';?>">
						<label class="input-group-addon">Message / Question<span>*</span></label>
						<textarea name="message" id="message" class="form-control" rows="8"><?php echo set_value('message'); ?></textarea>
						
						<?php echo form_error('message'); ?>
					  </div>
					  
					  <div class="formsparator" style="padding:10px 0">
						<div class="form-group">
						  <label >Input : <?php echo $cpt_code;?>  in textbox below :</label>
						</div>
						<div class="form-group <?php echo (form_error('captcha'))?'has-error':'';?>">
						  <input type="text" class="form-control" placeholder="Captcha *"  name="captcha" id="captcha" value="" maxlength="10" autocomplete="off" />
						  <?php echo form_error('captcha'); ?> </div>
					  </div>
					  <div align="center">
						<input type="submit" name="submit_button" id="submit_button" value="Submit" class="btn btn-success" />
					  </div>
					</div>
				  </div>
				</div>
				<!--/Job Detail--> 
				<?php echo form_close();?>
				<?php #$this->load->view('common/right_ads');?>
			  </div>
			</div>
		</div>
	</div>
</section>
</div>
<?php #$this->load->view('common/bottom_ads');?>
<!--Footer-->
<?php $this->load->view('common/footer'); ?>
<?php $this->load->view('common/before_body_close'); ?> 
</body>
</html>
