<!DOCTYPE html>
<html lang="en">
<head>
<?php $this->load->view('common/meta_tags'); ?>
<title><?php echo $title;?></title>
<?php $this->load->view('common/before_head_close'); ?>
</head>
<body class="theme-style-1">
<?php $this->load->view('common/after_body_open'); ?>
<div class="wrapper">
<!--Header-->
<?php $this->load->view('common/header'); ?>
<!--/Header--> 
<section id="inner-banner">

    <div class="container">

      <h1>Login To Your Account</h1>

    </div>

</section>
<!--Detail Info-->
<div id="main">
<section class="signup-section">
<div class="container">
 
  <div class="holder"> 
    
    <!--Signup-->
	<div class="thumb"><img src="<?php echo base_url("public/assets"); ?>/images/signup.png" alt="img"></div>
    <!--div class="col-md-6 col-md-offset-3" -->
     <!--Login-->
    <form name="login_form" id="login_form" action="" method="post">
      <div class="loginbox">
        <?php if($msg):?>
        	<div class="alert alert-danger"><?php echo $msg;?></div>
        <?php endif;?>
        <?php echo validation_errors(); ?>
        <?php echo $this->session->flashdata('success_msg');?>
        <div class="row">
          <div class="input-box"> <i class="fa fa-user"></i>
            <input type="text" name="email" id="email" class="form-control" value="<?php echo set_value('email'); ?>" placeholder="Email" />
          </div>
        </div>
        <div class="row">
          <div class="input-box"> <i class="fa fa-unlock"></i>
            <input type="password" name="pass" id="pass" autocomplete="off" value="<?php echo set_value('pass'); ?>" class="form-control" placeholder="Password" />
          </div>
        </div>
        <div class="row">
         <div class="check-box">
            <input type="checkbox">
            <strong>Remember Me</strong> 
        </div>
        <input type="submit" value="Sign In" class="btn btn-success" />
        </div>
		<a href="<?php echo base_url('forgot');?>" class="login">Forgot your Password</a> <b>OR</b>       
            <div class="login-social">

              <h4>Log in with Your Social Account</h4>

              <ul>

                <li> <a href="#" class="fb"><i class="fa fa-facebook"></i>Facebook Login</a> </li>

                <li> <a href="#" class="gp"><i class="fa fa-google-plus"></i>Google Plus Login</a> </li>

                <li> <a href="#" class="tw"><i class="fa fa-twitter"></i>Twitter Login</a> </li>

              </ul>
              <em>You Don’t have an Account? <a href="<?php echo $signup_link;?>">SIGN UP NOW</a></em> </div>
      </div>
    </form>
    
      
    <!--/div -->
    <!--/Login--> 

  </div>
</div>
<!--Footer-->
</section>
</div>
<?php $this->load->view('common/footer'); ?>
<?php $this->load->view('common/before_body_close'); ?>
</body>
</html>